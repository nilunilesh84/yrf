/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import LoggedIn from './Pages/LoggedIn';
import PushNotification, { Importance } from "react-native-push-notification";
import { useDispatch } from 'react-redux';
import { getAllRewards } from './Pages/redux/Actions/RewardsAction'
import { requestUserPermission, notificationListner } from './Pages/Notification/Notifications'
import AsyncStorageStatic from '@react-native-community/async-storage'
import { checkLogin, sharedTicketDetails } from './Pages/redux/Actions/userAction';
import ForegroundHandler from './Pages/Notification/ForegroundHandler';
import dynamicLinks from '@react-native-firebase/dynamic-links';
export default function App() {
  const dispatch = useDispatch()
  const fetchToken = async () => {
    const token = await AsyncStorageStatic.getItem('User');
    dispatch(checkLogin(token))
  }
  const getAppLaunchLink = async () => {
    try {
      const { url } = await dynamicLinks().getInitialLink();
      getQueryParams(url)

      console.log({ url })
    } catch (error) {
      // console.log(error, 'errors')
    }
  };
  const handleDynamicLink = link => {
    getQueryParams(link.url)
    console.log({ link })
  };
  function getQueryParams(url) {
    var qparams = {},
      parts = (url || '').split('?'),
      qparts, qpart,
      i = 0;
    if (parts.length <= 1) {
      return qparams;
    } else {
      qparts = parts[1].split('&');
      for (i in qparts) {
        qpart = qparts[i].split('=');
        qparams[decodeURIComponent(qpart[0])] =
          decodeURIComponent(qpart[1] || '');
      }
    }
    return dispatch(sharedTicketDetails(qparams));
  };
  useEffect(() => {
    const unsubscribe = dynamicLinks().onLink(handleDynamicLink);
    dispatch(getAllRewards())
    fetchToken()
    getAppLaunchLink()
    requestUserPermission()
    notificationListner()
    return () => unsubscribe()
  }, [])
  useEffect(() => {
    PushNotification.createChannel(
      {
        channelId: "channel-id", // (required)
        channelName: "My channel", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        playSound: false, // (optional) default: true
        soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
        importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
    // const remoteMessages = { "collapseKey": "com.yrf", "data": { "id": "627256f323fa4b0009fe9eec", "image": "https://cdn.fasalsetu.com/products/9d6c8a65-b6b7-40fa-9721-c3d486d41233.png", "queryParams": "", "route": "/orders", "type": "reward-mgmt" }, "from": "137903595327", "messageId": "0:1651660531807772%3889968038899680", "notification": { "android": { "imageUrl": "", "sound": "default" }, "body": "You have received your first 100 points!", "title": "Congratulations!" }, "sentTime": 1651660531781, "ttl": 2419200 }
    // const { notification, messageId, data } = remoteMessages
    // setTimeout(() => {
    //   PushNotification.localNotification({
    //     channelId: 'channel-id',
    //     color: 'green',
    //     body: notification.body,
    //     title: notification.title,
    //     message: notification.body,
    //     vibrate: true,
    //     vibration: 300,
    //     playSound: true,
    //     bigPictureUrl: data.image
    //   })
    //   console.log('first')
    // }, 5000)
    PushNotification.getChannels(function (channel_ids) {
      console.log(channel_ids); // ['channel_id_1']
    });
  }, [])
  return (
    <NavigationContainer>
      <ForegroundHandler />
      <LoggedIn />
    </NavigationContainer>
  )
}

