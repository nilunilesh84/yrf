/* eslint-disable prettier/prettier */
/**
 * @format
 */
import * as React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { Provider } from 'react-redux'
import { Store } from './Pages/redux/store';
import { name as appName } from './app.json';
import { Provider as PaperProvider } from 'react-native-paper'

import PushNotification from "react-native-push-notification";
import messaging from '@react-native-firebase/messaging';
// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
  });
//   messaging().setBackgroundMessageHandler(async remoteMessage => {
//     console.log('Message handled in the background handler!', remoteMessage);
//     const { notification, messageId, data } = remoteMessage
//     PushNotification.localNotification({
//         channelId: 'channel-id',
//         color: 'green',
//         body: notification.body,
//         title: notification.title,
//         message: notification.body,
//         vibrate: true,
//         vibration: 300,
//         playSound: true,
//         bigPictureUrl: data.image
//     })
// });
export default function Main() {
  return (
    <PaperProvider>
      <Provider store={Store}>
        <App />
      </Provider>
    </PaperProvider>
  );
}


AppRegistry.registerComponent(appName, () => Main);
