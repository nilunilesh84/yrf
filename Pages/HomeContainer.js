import React from 'react';
import { View, Dimensions, TouchableOpacity } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import Home from './Home';
import CustomDrawer from './Screens/CustomDrawer';

import { useDispatch } from 'react-redux';
import { openDrawer, } from '../Pages/redux/Actions/action';
import { COLORS, SIZES } from './Constants/theme';

const Drawer = createDrawerNavigator();
export default function HomeContainer({ navigation }) {
  const dispatch = useDispatch();
  function HomeHeader() {
    return (
      <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
        <TouchableOpacity style={{ margin: 11 }} onPress={() => navigation.navigate('QrScanner')} >
          <Entypo name="upload" size={24} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity style={{ margin: 11 }} onPress={() => dispatch(openDrawer())}>
          <FontAwesome5 name="user-plus" size={24} color="#fff" />
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawer {...props} />}
      screenOptions={{
        swipeEnabled: false,
        headerStyle: {
          backgroundColor: COLORS.primary,
        },
        drawerStyle: {
          width: SIZES.width - 60,
        },
        drawerLabelStyle: {
          color: '#fff',
          fontSize: 16,
        },
        headerMode: null,
        drawerLabel: () => null,
        headerTintColor: 'white',
        drawerActiveTintColor: COLORS.primary,
      }}
    >
      <Drawer.Screen name="HomePage" component={Home} options={{
        headerTitleStyle: {
          color: COLORS.primary,
        },
        headerRight: (props) => <HomeHeader {...props} />,
      }} />
    </Drawer.Navigator>
  );
}

