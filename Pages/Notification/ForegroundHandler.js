import { useEffect } from "react";
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";


const ForegroundHandler = () => {
    useEffect(() => {
        const unsubscribe = messaging().onMessage((remoteMessage) => {
            console.log('handle in foreground', remoteMessage)
            const { notification, messageId, data } = remoteMessage
            PushNotification.localNotification({
                channelId: 'channel-id',
                color: 'green',
                body: notification.body,
                title: notification.title,
                message: notification.body,
                vibrate: true,
                vibration: 300,
                playSound: true,
                bigPictureUrl: data.image
            })
        })
        return unsubscribe
    }, [])
 
    return null
}
export default ForegroundHandler