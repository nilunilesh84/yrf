import React, { useEffect, useState } from 'react'
import {
    View, FlatList, Text, StyleSheet,
    TouchableOpacity, BackHandler
} from 'react-native';
import axios from 'axios';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Caption } from "react-native-paper";
import { useSelector, useDispatch } from 'react-redux'
import { COLORS, FONTS, SIZES } from '../../Constants/theme';
import HistorySingleComponent from '../../Components/HistorySingleComponent';
import { ticketDelete } from '../../redux/Actions/TicketsAction';

function History({ navigation }) {
    const dispatch = useDispatch()
    const { history, newPoints } = useSelector(state => state.user)
    let mapData = history?.filter((item) => item.eventName !== 'UNCLAIMED_POINTS' || null)
    let refData = history?.filter((item) => item.eventName === 'Referral')
    const data = mapData?.map(item => item?.ticketId?.price || item.points).filter(item => item > 0)
    const refDataPoints = refData?.map(item => item.points)
    const reducer = (previousValue, currentValue) => previousValue + currentValue;
    let totalrefDataPoints = 0
    try {
        totalrefDataPoints = refDataPoints.reduce(reducer)
    } catch (error) {

    }

    const setImage = async (data) => {
        if (data?.movieName) {
            const results = await axios.get(`https://imdb-api.com/en/API/Search/k_8qg681nx/${data.movieName}`)
            if (results) {
                const resultImage = results.data.results[0].image
                await axios.put(`https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/tickets/${data._id}`, { ticketImage: resultImage })
            }
        }
        return
    }
    const reload = () => {
        const imageCheck = mapData?.filter(item => item.ticketId).map(item => item.ticketId)
        const imagehistory = imageCheck?.filter(item => !item.ticketImage)
        imagehistory?.map(item => setImage(item))
    }
    useEffect(() => {
        reload()
    }, [])
    useEffect(() => {
        dispatch(ticketDelete())
        const backAction = () => {
            navigation.navigate("Home")
            return true;
        };
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
        );
        return () => backHandler.remove();
    }, []);
    const renderItem = ({ item }) => (
        <HistorySingleComponent item={item} />
    )
    const [loading, setloading] = useState(false);

    return (
        <View style={styles.body}>
            <View style={styles.headerStyle}>
                <TouchableOpacity onPress={() => navigation.navigate('HomePage')} >
                    <AntDesign name='arrowleft' size={25} color='#fff' />
                </TouchableOpacity>
                <Text style={{ ...FONTS.font18, fontSize: 22, paddingRight: '40%' }}>
                    History
                </Text>
            </View>
            <View style={{ width: SIZES.width, padding: 12 }}>
                <View style={styles.box}>
                    <Caption style={{ ...FONTS.font18, fontSize: 21 }}>Last Earned</Caption>
                    <Text style={{ ...FONTS.font18, fontSize: 21 }}>{data[0] || '0'} Pts</Text>
                </View>
                <View style={styles.box}>
                    <Caption style={{ ...FONTS.font18, fontSize: 21 }}>Referal earned</Caption>
                    <Text style={{ ...FONTS.font18, fontSize: 21 }}>{totalrefDataPoints || 0} Pts</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', top: 10, marginBottom: 10 }}>
                    <View style={{ flex: 1, height: 1, backgroundColor: '#ADADB2' }} />
                </View>
                <View style={styles.box}>
                    <Caption style={{
                        ...FONTS.font18,
                        color: COLORS.purple, fontSize: 21,
                    }}>Total Points</Caption>
                    <Text style={{
                        ...FONTS.font18,
                        color: COLORS.purple, fontSize: 21,
                    }}>{parseInt(newPoints)} Pts</Text>
                </View>
            </View>
            <View style={styles.lowerWrapper}>
                <View style={{
                    padding: 10, marginLeft: 'auto', right: 8
                }}>
                    <TouchableOpacity onPress={() => navigation.navigate('HistoryViewDetails')}>
                        <Text style={{ ...FONTS.font18, color: COLORS.darkpurple, }}>View Details</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={mapData}
                    initialNumToRender={10}
                    renderItem={renderItem}
                    keyExtractor={item => item._id}
                    refreshing={loading}
                    onRefresh={() => reload()}
                />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: COLORS.background,
        alignItems: 'center',
        height: SIZES.height,
    },
    box: {
        alignItems: 'center',
        padding: 6,
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 10
    },
    lowerWrapper: {
        width: SIZES.width,
        minHeight: '73%',
        maxHeight: '73%',
        backgroundColor: '#ffff',
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        paddingBottom: 65,
        padding: 6
    }, headerStyle: {
        flexDirection: 'row',
        top: 14,
        marginBottom: 14,
        justifyContent: 'space-between',
        width: SIZES.width * .95,
        alignItems: 'center',
    }
})
export default History
