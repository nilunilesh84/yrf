import React, { useEffect, useState } from 'react';
import { StyleSheet, View, } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import { UpdatePoints } from '../../redux/Actions/PointsAction';
import { UpdateUser } from '../../redux/Actions/userAction';
import CustomButton from '../../Components/CustomButton'
import { RadioButton, Text } from 'react-native-paper';
import CustomSnack from '../../Components/CustomSnack';

const Gender = () => {
    const dispatch = useDispatch()
    const [text, settext] = useState('')
    const [valueGender, setvalueGender] = useState('')
    const [dialogVisible, setdialogVisible] = useState(false)
    const { userData } = useSelector(state => state.user)
    useEffect(() => {
        userData?.gender && setvalueGender(userData?.gender)
    }, [])
    const submit = () => {
        if (userData?.gender === null) {
            dispatch(UpdatePoints({ points: 50, eventName: "GENDER_POINTS" }))
        }
        return
    }
    const allValues = () => {
        dispatch(UpdateUser({ valueGender, value: 'gender' })),
            submit(), setdialogVisible(true),
            settext('Gender Updated Successfully')
        return
    }
    return (
        <View style={styles.body}>
            <RadioButton.Group onValueChange={newValue => setvalueGender(newValue)} value={valueGender}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <RadioButton value="Male" />
                        <Text style={styles.text}>Male</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <RadioButton value="Female" />
                        <Text style={styles.text}>Female</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <RadioButton value="Others" />
                        <Text style={styles.text}>Others</Text>
                    </View>
                </View>
            </RadioButton.Group>
            <CustomButton disabled={!Boolean(valueGender)} btntext={userData?.gender ? "Update" : 'Add'} onPress={allValues} />
            <CustomSnack style={{ bottom: 0 }} success={true} snackVisible={dialogVisible} setSnackVisible={setdialogVisible} snacktext={'Gender Updated Successfully'} />
        </View>
    );
};

export default Gender;

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center'
    }, text: {
        color: '#fff',
        fontSize: 13,
        marginRight: 9
    },
});

//disabled={!Boolean(isCheckedmale.bollean || isCheckedOthers.bollean || isCheckedFemale.bollean)}