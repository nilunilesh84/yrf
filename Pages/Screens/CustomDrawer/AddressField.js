import React, { useEffect, useState } from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import { UpdatePoints } from '../../redux/Actions/PointsAction';
import { UpdateUser } from '../../redux/Actions/userAction';
import CustomSnack from '../../Components/CustomSnack';
import CustomButton from '../../Components/CustomButton';
import { COLORS } from '../../Constants/theme';
const AddressField = () => {
    const { userData } = useSelector(state => state.user)
    const apidata = JSON.parse(userData.address)
    const [details, setdetails] = useState('')
    const [dialogVisible, setdialogVisible] = useState(false)
    const [landmark, setlandmark] = useState('')
    const [pincode, setpincode] = useState('')
    const [city, setcity] = useState('')
    const [stateof, setstateof] = useState('')
    useEffect(() => {
        apidata &&
            (setpincode(apidata.pincode),
                setlandmark(apidata.landmark),
                setcity(apidata.city),
                setstateof(apidata.state),
                setdetails(apidata.details))
    }, [])
    const fullAddress = {
        details,
        landmark,
        pincode,
        city,
        stateof
    }
    const dispatch = useDispatch()
    const submit = () => {
        if (apidata === null) {
            dispatch(UpdatePoints({ points: 50, eventName: "ADDRESS_POINTS" }))
        }
        return
    }
    const allValues = () => {
        {
            if (valid) {
                (dispatch(UpdateUser({ fullAddress, value: 'address' })), submit(), setdialogVisible(true))
            }
        }
        return
    }
    const valid = details.length && landmark.length && pincode.length && city.length && stateof.length
    return (
        <View style={styles.container}>
            <TextInput onChangeText={value => setdetails(value)} value={details} placeholderTextColor='#5A5A5A' placeholder='Details' style={styles.textinput2} />
            <View style={{ flexDirection: 'row', }}>
                <TextInput onChangeText={value => setlandmark(value)} value={landmark} placeholderTextColor='#5A5A5A' placeholder='Landmark' style={styles.textinput} />
                <TextInput onChangeText={value => setstateof(value)} value={stateof} placeholderTextColor='#5A5A5A' placeholder='State' style={styles.textinput} />
            </View>
            <View style={{ flexDirection: 'row' }}>
                <TextInput value={city} onChangeText={value => setcity(value)}
                    placeholderTextColor='#5A5A5A' placeholder='City' style={styles.textinput} />
                <TextInput keyboardType='numeric' onChangeText={value => setpincode(value)} value={pincode} placeholderTextColor='#5A5A5A' placeholder='Pincode' style={styles.textinput} />
            </View>
            <CustomButton btntext={userData?.address ? "Update" : 'Add'} onPress={allValues} />
            <CustomSnack success={true} snackVisible={dialogVisible} setSnackVisible={setdialogVisible} snacktext={'Address Updated Successfully'} />
        </View>
    );
};

export default AddressField;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    textinput: {
        width: 120,
        padding: 12,
        margin: 3,
        height: 37,
        borderRadius: 8,
        backgroundColor: "#1C1A29",
        alignItems: 'center',
        fontSize: 13,
        color: '#fff',
    },
    textinput2: {
        padding: 12,
        width: 245,
        margin: 3,
        height: 37,
        borderRadius: 8,
        backgroundColor: "#1C1A29",
        fontSize: 13,
        color: '#fff',
        fontWeight: '500'
    },
    btn: {
        backgroundColor: COLORS.secondary,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        borderRadius: 10,
        height: 40,
        alignSelf: 'center',
        width: 118,
    }
});