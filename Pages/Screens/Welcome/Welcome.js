/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Dimensions, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { COLORS, FONTS, SIZES } from '../../Constants/theme';
import { scale, moderateScale, moderateVerticalScale, verticalScale } from 'react-native-size-matters'


export default function Welcome({ navigation }) {
  return (
    <View style={styles.body}>
      <View style={styles.topWrapper}>
        <Text style={styles.fonts1}>Welcome To</Text>
      </View>
      <View style={styles.lowerWrapper}>
        <Image resizeMode='stretch' style={{ width: SIZES.width - 140, height: 80, bottom: 50 }}
          source={require('../../../assets/images/Capture.jpg')} />
        <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 40, marginTop: 20 }}>
          <Text style={styles.fonts1}>Reward earning</Text>
          <Text style={styles.font2}>platform</Text>
        </View>
        <TouchableOpacity style={{}} onPress={() => navigation.navigate('Login')}>
          <View style={styles.btn}>
            <Text style={{ color: '#fff', fontSize: 22, fontWeight: '500' }}>Get Started</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: COLORS.background,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  lowerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fonts1: {
    ...FONTS.font18,
    fontSize: moderateScale(35),
    alignSelf: 'center',
    margin: moderateScale(5),
    bottom: moderateScale(10)
  },
  font2: {
    ...FONTS.font18,
    fontSize: moderateScale(34),
    color: '#BA9DFF',
    bottom: moderateScale(10),
  },
  btn: {
    backgroundColor: COLORS.primary,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(8),
    height: verticalScale(55),
    width: scale(250),
    bottom: verticalScale(20)
  }
})

