import { StyleSheet } from "react-native"
import { ScaledSheet, verticalScale, moderateScale, scale } from "react-native-size-matters"
import { SIZES, COLORS } from "../../Constants/theme"


export const styles = ScaledSheet.create({
    body: {
        flex: 1,
        backgroundColor: COLORS.background,
        alignItems: 'center',
        padding: '9@s',
        width: (SIZES.width)
    },
    imageStyle: {
        width: SIZES.width * .94,
        height: SIZES.height * .4,
        overflow: 'hidden',
        borderRadius: 8,
        marginTop: '10@s',
    },
    lowerWrapper: {
        padding: '6@s',
        paddingBottom: '80@s',
    },
    btn: {
        backgroundColor: COLORS.primary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        height: 55,
        width: SIZES.width * .8,
    },
    claimedContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 15,
        width: SIZES.width * .94,
    },
    btnS: {
        backgroundColor: COLORS.background,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 60,
        width: SIZES.width * .3,
        borderWidth: 2,
        borderStyle: 'dashed',
        borderColor: COLORS.purple,
    },
})