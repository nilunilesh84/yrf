import React, { useEffect } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity,
  FlatList, Image,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import Coin from '../../Components/Coin';
import { FONTS, SIZES } from '../../Constants/theme';
import { changeRefresh } from '../../redux/Actions/action';

export default function ViewAll({ navigation }) {

  const { rewards } = useSelector(state => state.rewards)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(changeRefresh(false))
  }, [])

  const renderItem1 = ({ item }) => (
    <TouchableOpacity style={{
      width: SIZES.width * .45,
      margin: 6
    }}
      onPress={() => navigation.navigate('RewardDetails', {
        itemDetail: item
      })} >
      <Image style={styles.smallImage} resizeMode='stretch' source={{ uri: item.bannerImage }} />
      <Text style={{ ...FONTS.font16, left: 5 }}>{item.rewardName.substring(0, 20)}</Text>
      <Coin points={item.points} />
    </TouchableOpacity>
  )
  return (
    <View style={styles.body}>
      <FlatList
        data={rewards}
        numColumns={2}
        renderItem={renderItem1}
        keyExtractor={item => item._id}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: '#201E2D',
    paddingTop: 15,
    alignItems: 'center',
    padding: 6,
  },
  smallImage: {
    width: '100%',
    height: 200,
    overflow: 'hidden',
    borderRadius: 7,
  },

})

