import React from "react";
import { View, StyleSheet, } from 'react-native'

import { useSelector } from 'react-redux'
import { COLORS, SIZES } from "../../Constants/theme";
import FileTicket from './Ticket'
import CustomLoader from "../../Components/CustomLoader";
import Opsscanned from '../../QrContainer/Opsscanned'

export default function Ticket() {
    const {  errorofSms, error } = useSelector(state => state.ticket)
    const { loadingsms } = useSelector(state => state.loading)
    return (
        <View style={styles.body}>
            {
                !loadingsms ? (
                    (error || errorofSms) ?
                        <Opsscanned />
                        :
                        <FileTicket />
                ) : <CustomLoader />
            }
        </View>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: COLORS.background,
        padding: 8,
        width: SIZES.width,
        height: SIZES.height,
        alignItems: 'center',
        justifyContent: 'center'
    },
})