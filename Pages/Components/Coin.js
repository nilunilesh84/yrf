import { View, Text,ImageBackground,Image } from 'react-native';
import React from 'react';

const Coin = ({points}) => {
  return (
    <View style={{ flexDirection: 'row', left: 5, top: 5 }}>
    <ImageBackground style={{ width: 19, height: 19, marginRight: 12 }} source={require('../../assets/images/Vector.png')}>
      <Image style={{ width: 10, height: 10 , top: 5, left: 4 }} resizeMode='cover' source={require('../../assets/images/Ve.png')} />
    </ImageBackground>
    <Text style={{ color: '#ffff', fontWeight: '500', fontSize: 15 }}>
      {`${points} Pts`}
    </Text>
  </View>
  );
};

export default Coin;
