/* eslint-disable prettier/prettier */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from "react";
import {
  Animated,
  Dimensions,
  Pressable,
  StyleSheet,
  View,Image,Text,
} from "react-native";
import { PanGestureHandler } from "react-native-gesture-handler";
import { IconButton, Portal } from "react-native-paper";
import Indicator2 from "./Indicator2";
const Sheet = ({ show, onDismiss, enableBackdropDismiss, children,onPress }) => {
  const bottomSheetHeight = Dimensions.get("window").height * 0.6;
  const deviceWidth = Dimensions.get("window").width;
  const [open, setOpen] = useState(show);
  const bottom = useRef(new Animated.Value(-bottomSheetHeight)).current;

  const onGesture = (event) => {
    if (event.nativeEvent.translationY > 0) {
      bottom.setValue(-event.nativeEvent.translationY);
    }
  };

  const onGestureEnd = (event) => {
    if (event.nativeEvent.translationY > bottomSheetHeight / 2) {
      onDismiss();
    } else {
      bottom.setValue(0);
    }
  };

  useEffect(() => {
    if (show) {
      setOpen(show);
      Animated.timing(bottom, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(bottom, {
        toValue: -bottomSheetHeight,
        duration: 500,
        useNativeDriver: false,
      }).start(() => {
        setOpen(false);
      });
    }
  }, [show]);

  if (!open) {
    return null;
  }
  return (
    <Portal>
      <Pressable
        onPress={enableBackdropDismiss ? onDismiss : undefined}
        style={styles.backDrop}
      />
      <Animated.View
        style={[
          styles.root,
          {
            height: bottomSheetHeight,
            bottom: bottom,
            shadowOffset: {
              height: -3,
            },
          },
          styles.common,
        ]}
      >
        <PanGestureHandler onGestureEvent={onGesture} onEnded={onGestureEnd}>
          <View
            style={[
              //   styles.header,
              styles.common,
              {
                position: "relative",
                shadowOffset: {
                  height: 3,
                },
              },
            ]}
          >
            <View
              style={{
                width: 60,
                height: 3,
                borderRadius: 3.5,
                position: "absolute",
                top: 8,
                left: (deviceWidth - 60) / 2,
                zIndex: 10,
                backgroundColor: "#ffff",
              }}
            />
            <IconButton
              icon="close" size={35}
              style={styles.closeIcon}
              onPress={onDismiss}
            />
          </View>
        </PanGestureHandler>
        <View style={{ alignItems: 'center', marginTop: 30 }}>
          <Image style={styles.image} source={require('../../assets/images/5star.jpg')} />
          <View style={{ flexDirection: 'row', width: 240, justifyContent: 'space-between' }}>
            <Indicator2 />
            <View>
              <Text style={{ color: 'black', fontWeight: '500', fontSize: 14 }}>Refera friend</Text>
              <Text style={{ color: 'black', fontWeight: '500', fontSize: 14 }}>Friends Uses This App</Text>
              <Text style={{ color: 'black', fontWeight: '500', fontSize: 14 }}>You get <Text style={{ color: 'purple' }}>100pts </Text>they get <Text style={{ color: 'purple' }}>50pts</Text> </Text>
            </View>
          </View>
          <Pressable style={{ width: '45%', top: 30, height: 45 }} onPress={onPress}>
            <View style={styles.btn}>
              <Text style={{ color: 'white', fontWeight: '500', fontSize: 16 }}>Refer Now</Text>
            </View>
          </Pressable>
        </View>
      </Animated.View>
    </Portal>
  );
};

const styles = StyleSheet.create({
  root: {
    position: "absolute",
    left: 0,
    right: 0,
    zIndex: 100,
    backgroundColor: "#fff",
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    overflow: "hidden",
  },
  header: {
    height: 44,
    backgroundColor: "#fff",
  },
  btn: {
    backgroundColor: '#1C1A29',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 54,
    height: 42,
    width: '100%',
    shadowOpacity: 0.9,
    elevation: 10,
  },
  common: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
    },
    shadowOpacity: 0.24,
    shadowRadius: 4,
    elevation: 3,
  },
  closeIcon: {
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 20,
    fontSize: 20
  },
  backDrop: {
    ...StyleSheet.absoluteFillObject,
    zIndex: 80,
    backgroundColor: "rgba(0,0,0, 0.12)",
  },
});

export default Sheet;