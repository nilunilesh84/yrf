import React, { useEffect } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { FONTS, SIZES } from '../Constants/theme'
import { changeError } from '../redux/Actions/FileuploadAction'


const Opsscanned = () => {
    const dispatch = useDispatch()
    const { errorofSms, error, } = useSelector(state => state.ticket)
    useEffect(() => {
        return () => (
            dispatch(changeError())
        )
    }, [])
    return (
        <View style={styles.body}>
            <View style={styles.smallImage}>
                <Text style={{ ...FONTS.font18, fontSize: 32, }}>Oh no !</Text>
                {
                    (error || errorofSms) == 'Duplicate scan' || 'Tickets are already  scanned' ?
                        <View>
                            <Text style={{ ...FONTS.font18, fontSize: 20, }}>This ticket has </Text>
                            <Text style={{ ...FONTS.font18, fontSize: 20, }}>already Scanned </Text>
                        </View>
                        : <>
                            <Text style={{ ...FONTS.font18, fontSize: 20, top: 10 }}>SomeThing Went Wrong  </Text>
                        </>
                }
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    smallImage: {
        width: SIZES.width * .8,
        height: 275,
        borderRadius: 30,
        padding: 10,
        backgroundColor: '#33313F',
        alignItems: 'center',
        justifyContent: 'center'
    }
    ,
})
export default Opsscanned
