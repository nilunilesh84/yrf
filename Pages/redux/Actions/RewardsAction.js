import axios from 'axios'
import moment from 'moment';
export const SET_REWARDS = 'SET_REWARDS'


export const getAllRewards = () => async dispatch => {
    var config = {
        method: 'get',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/rewards',
     };
    try {
        const { data } = await axios.get(config.url)
        var currentdate = moment()
            .utcOffset('+05:30')
            .format('YYYY-MM-DD');
        const mapRewards = data.data?.filter(item => item.deleted !== true && new Date(item.validity) >= new Date(currentdate))
        dispatch({ type: SET_REWARDS, payload: mapRewards })
    } catch (error) {
        console.log(error.response)
    }

}