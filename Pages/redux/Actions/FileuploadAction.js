import axios from "axios";
import { hideLoader, showLoader } from "./loaderAction";
export const FILE_TICKETS = 'FILE_TICKETS'
export const ERROR = 'ERROR'
export const SHOW_LOADER = "SHOW_LOADER";
export const HIDE_LOADER = "HIDE_LOADER";

export const imageUpload = (uploadData) => async dispatch => {
    dispatch(showLoader())
    const Imagedata = new FormData();
    const name = uploadData.image.path.split("/")
    Imagedata.append('file', {
        uri: uploadData.image.path,
        type: uploadData.image.mime,
        name: name[name.length - 1]
    });
    Imagedata.append('From', `${uploadData.phoneNo}`)
    var config = {
        method: 'post',
        url: 'http://3.110.83.166:8080/parseticket',
    };
    try {
        const { data } = await axios.post(config.url, Imagedata)
        if (data.error) {
            dispatch({ type: 'SCANNED_FAILED', payload: data.error })
        }
        if (data?.movieName) {
            const results = await axios.get(`https://imdb-api.com/en/API/Search/k_8qg681nx/${data.movieName}`)
            if (results) {
                const resultImage = results.data.results[0].image
                const result = await axios.put(`https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/tickets/${data._id}`, { ticketImage: resultImage })
                if (result.data) {
                    dispatch({ type: FILE_TICKETS, payload: result.data })
                } else if (data) {
                    dispatch({ type: FILE_TICKETS, payload: data })
                }
            }
        }
        else {
            dispatch({ type: FILE_TICKETS, payload: data })
            // dispatch({ type: 'SCANNED_FAILED', payload: data.error })
        }
        dispatch(hideLoader())
    } catch (error) {
        console.log(error, 'imageUpload error')
        dispatch(hideLoader())
        dispatch({ type: 'SCANNED_FAILED', payload: error })
    }
}
export const changeError = () => dispatch => {
    dispatch({ type: ERROR })
}
