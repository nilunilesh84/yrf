
import {
    OPEN_DRAWER, CLOSE_DRAWER,
    REFRESH,
} from "../Actions/action";


const initState = {
    show: null,
    refresh: false,
}

export default function Reducer(state = initState, action) {
    const { type, payload } = action
    switch (type) {
        case OPEN_DRAWER:
            return { ...state, show: true }
        case CLOSE_DRAWER:
            return { ...state, show: false }
        case REFRESH:
            return { ...state, refresh: payload }
        default:
            return state

    }
}