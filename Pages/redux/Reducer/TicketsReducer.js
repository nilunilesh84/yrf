import {
    GET_TICKETS_BY_ID, TICKET_DELETE
} from '../Actions/TicketsAction'
import { FILE_TICKETS, ERROR } from '../Actions/FileuploadAction'
import { LOGOUT } from '../Actions/userAction'

const initState = {
    fileTicket: null,
    error: null,
    errorofSms: null,
    sharedTicketwithId: null,
}
export default function TicketsReducer(state = initState, action) {
    const { type, payload } = action
    switch (type) {
        case ERROR:
            return { ...state, error: null, errorofSms: null, }
        case LOGOUT:
            return { ...state, sharedTicketwithId: null }
        case FILE_TICKETS: return {
            ...state, fileTicket: payload,
        }
        case GET_TICKETS_BY_ID: return {
            ...state, fileTicket: payload,
        }
        case 'SCANNED_FAILED':
            return {
                ...state, error: payload,
            }
        case 'SCANNED_FAILED_SMS':
            return {
                ...state, errorofSms: payload,
            }
        case 'SHARED_ID_SAVE':
            return {
                ...state, sharedTicketwithId: payload,
            }
        case TICKET_DELETE: return {
            ...state, fileTicket: null,
            sharedTicketwithId: null, error: null, errorofSms: null,
        }
        default:
            return state

    }
}